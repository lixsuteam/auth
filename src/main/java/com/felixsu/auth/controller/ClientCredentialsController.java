package com.felixsu.auth.controller;

import com.felixsu.commons.controller.BaseController;
import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.auth.service.ClientCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created on 12/13/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("credential")
public class ClientCredentialsController extends BaseController {

    private ClientCredentialsService service;

    @Autowired
    public ClientCredentialsController(HttpServletRequest request, ClientCredentialsService service) {
        super(request);
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> save(@RequestBody BaseClientDetails clientDetails) throws BadRequestException {
        service.save(clientDetails);
        return new ResponseEntity<>("{}", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<String> update(@RequestBody BaseClientDetails clientDetails) throws NotFoundException, BadRequestException {
        service.update(clientDetails);
        return new ResponseEntity<>("{}", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{clientId}")
    public ClientDetails findOne(@PathVariable String clientId) throws NotFoundException {
        return service.loadByClientId(clientId);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ClientDetails> findAll() {
        return service.loadAll();
    }
}
