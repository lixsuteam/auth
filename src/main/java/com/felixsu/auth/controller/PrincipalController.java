package com.felixsu.auth.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created on 12/6/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "principal")
public class PrincipalController {

    @RequestMapping
    public Principal getPrincipalDetails(Principal principal) {
        return principal;
    }
}
