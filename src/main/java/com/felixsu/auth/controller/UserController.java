package com.felixsu.auth.controller;

import com.felixsu.commons.controller.BaseController;
import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.commons.model.PagingWrapper;
import com.felixsu.auth.model.SimpleUserDetail;
import com.felixsu.auth.service.SimpleUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created on 12/6/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "users")
public class UserController extends BaseController {

    private SimpleUserDetailService service;

    @Autowired
    public UserController(HttpServletRequest request, SimpleUserDetailService simpleUserDetailService) {
        super(request);
        this.service = simpleUserDetailService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public SimpleUserDetail save(@RequestBody SimpleUserDetail user) throws BadRequestException {
        return service.save(user);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}")
    public SimpleUserDetail update(@PathVariable Integer id, @RequestBody SimpleUserDetail user) throws NotFoundException, BadRequestException {
        return service.update(id, user);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{ud}")
    public SimpleUserDetail findOne(@PathVariable Integer id) throws NotFoundException {
        return service.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public PagingWrapper<SimpleUserDetail> findAll() {
        return service.findAll(getQueryParams(), null);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public SimpleUserDetail delete(@PathVariable Integer id) throws NotFoundException {
        return service.delete(id);
    }
}
