package com.felixsu.auth.controller;

import com.felixsu.auth.service.MyTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "token")
public class TokenController {

    private MyTokenService myTokenService;

    @Autowired
    public TokenController(MyTokenService myTokenService) {
        this.myTokenService = myTokenService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "revoke/{token}")
    public Map<String, Object> revokeToken(@PathVariable String token) {
        return myTokenService.revokeToken(token);
    }

    @RequestMapping(method = RequestMethod.POST, path = "revoke_refresh/{token}")
    public Map<String, Object> revokeRefreshToken(@PathVariable String token) {
        return myTokenService.revokeRefreshToken(token);
    }
}
