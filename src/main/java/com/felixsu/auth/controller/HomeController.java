package com.felixsu.auth.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created on 12/6/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("/")
public class HomeController {

    @RequestMapping
    public String getHome() {
        ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("UTC"));
        return String.format("auth service at your service, server time: %s", zdt.toString());
    }
}
