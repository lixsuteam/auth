package com.felixsu.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 * Created on 12/5/17.
 *
 * @author felixsoewito
 */
@SpringBootApplication(scanBasePackages = {"com.felixsu.commons", "com.felixsu.auth"})
@EntityScan(basePackageClasses = { Application.class, Jsr310JpaConverters.class })
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

