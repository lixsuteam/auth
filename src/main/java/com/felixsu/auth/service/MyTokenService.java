package com.felixsu.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 12/6/17.
 *
 * @author felixsoewito
 */
@Service
public class MyTokenService {
    @Qualifier("tokenServices")
    private ConsumerTokenServices tokenServices;

    @Qualifier("tokenStore")
    private TokenStore tokenStore;

    @Autowired
    public MyTokenService(ConsumerTokenServices tokenServices, TokenStore tokenStore) {
        this.tokenServices = tokenServices;
        this.tokenStore = tokenStore;
    }

    public Map<String, Object> revokeToken(String token) {
        boolean removed = tokenServices.revokeToken(token);

        Map<String, Object> result = new LinkedHashMap<>();
        result.put("isRemoved", removed);
        result.put("token", token);

        return result;
    }

    public Map<String, Object> revokeRefreshToken(String token) {
        boolean removed = false;
        if (tokenStore instanceof JdbcTokenStore) {
            ((JdbcTokenStore) tokenStore).removeRefreshToken(token);
            removed = true;
        }

        Map<String, Object> result = new LinkedHashMap<>();
        result.put("isRemoved", removed);
        result.put("token", token);

        return result;
    }

    public Collection<OAuth2AccessToken> getTokens(String clientId) {
        return tokenStore.findTokensByClientId(clientId);
    }


}
