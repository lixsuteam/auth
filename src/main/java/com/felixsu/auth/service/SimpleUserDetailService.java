package com.felixsu.auth.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.commons.model.PagingWrapper;
import com.felixsu.commons.service.BaseService;
import com.felixsu.auth.model.SimpleUserDetail;
import com.felixsu.auth.persistence.repository.UserDetailRepository;
import com.felixsu.auth.persistence.model.UserDetailEntity;
import com.felixsu.auth.model.SimpleUserPrincipal;
import com.felixsu.commons.util.ReflectionUtil;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created on 12/6/17.
 *
 * @author felixsoewito
 */
@Service
public class SimpleUserDetailService extends BaseService<UserDetailEntity, Integer> implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUserDetailService.class);

    private PasswordEncoder encoder;
    private UserDetailRepository repository;

    public SimpleUserDetailService(PasswordEncoder encoder, UserDetailRepository repository) {
        super(UserDetailEntity.class);
        this.encoder = encoder;
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetailEntity dao = repository.findByUsername(username);
        if (dao == null) {
            dao = repository.findByEmail(username);
        }

        if (dao == null) {
            throw new UsernameNotFoundException(String.format("user %s not found", username));
        }
        return new SimpleUserPrincipal(toDto(dao));
    }

    public SimpleUserDetail save(SimpleUserDetail item) throws BadRequestException {
        if (item.getPassword() == null) {
            throw new BadRequestException("Password must be filled");
        }
        item.setPassword(encoder.encode(item.getPassword()));
        item.setActive(true);

        UserDetailEntity dao = repository.save(toDao(item));

        return toDto(dao);
    }

    public SimpleUserDetail update(Integer id, SimpleUserDetail item) throws NotFoundException, BadRequestException {
        UserDetailEntity existing = findOneAsDao(id);
        if (item.getId() == null) {
            throw new BadRequestException("ID must not be null");
        }

        if (item.getPassword() != null) {
            item.setPassword(encoder.encode(item.getPassword()));
        }

        UserDetailEntity newInstance = toDao(item);

        try {
            LOGGER.debug(String.format("calling update partial for: %s", newInstance.getClass()));
            List<Field> classFields = ReflectionUtil.getAllFields(newInstance.getClass());
            for (Field field : classFields) {
                JsonProperty jsonProperty = field.getDeclaredAnnotation(JsonProperty.class);

                String setterName = jsonProperty != null
                        ? jsonProperty.value()
                        : field.getName();

                Object currentVal = PropertyUtils.getProperty(existing, setterName);
                Object newVal = PropertyUtils.getProperty(newInstance, setterName);
                PropertyUtils.setProperty(newInstance, setterName, getNonNull(newVal, currentVal));
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return toDto(repository.save(newInstance));
    }

    public SimpleUserDetail findOne(Integer id) throws NotFoundException {
        return toDto(findOneAsDao(id));
    }

    public SimpleUserDetail findByUsername(String username) throws NotFoundException {
        UserDetailEntity t = repository.findByUsername(username);
        if (t == null) {
            throw new NotFoundException(String.format("Item with username: %s not found", username));
        }
        return toDto(t);
    }

    public SimpleUserDetail findByEmail(String email) throws NotFoundException {
        UserDetailEntity t = repository.findByEmail(email);
        if (t == null) {
            throw new NotFoundException(String.format("Item with email: %s not found", email));
        }
        return toDto(t);
    }

    public SimpleUserDetail delete(Integer id) throws NotFoundException {
        SimpleUserDetail result = findOne(id);
        repository.delete(id);
        return result;
    }

    @SuppressWarnings("unchecked")
    public PagingWrapper<SimpleUserDetail> findAll(Map<String, String> queryStrings, Pageable pageable) {
        Page<UserDetailEntity> result = repository.findAll(
                buildSpec(queryStrings),
                pageable == null ? new PageRequest(0, 20) : pageable);

        PagingWrapper pagingWrapper = new PagingWrapper();
        pagingWrapper.setDatas(result.getContent().stream().map(dao -> toDto(dao)).collect(Collectors.toList()));
        pagingWrapper.setTotalElement(result.getTotalElements());
        pagingWrapper.setTotalPages(result.getTotalPages());
        pagingWrapper.setFirst(result.isFirst());
        pagingWrapper.setLast(result.isLast());
        return pagingWrapper;
    }

    private UserDetailEntity findOneAsDao(Integer id) throws NotFoundException {
        UserDetailEntity t = repository.findOne(id);
        if (t == null) {
            throw new NotFoundException(String.format("Item with id #%d not found", id));
        }
        return t;
    }

    private UserDetailEntity toDao(SimpleUserDetail userDetail) throws BadRequestException {
        UserDetailEntity dao = new UserDetailEntity();
        dao.setId(userDetail.getId());
        dao.setName(userDetail.getName());
        dao.setEmail(userDetail.getEmail());
        dao.setPassword(userDetail.getPassword());
        dao.setActive(userDetail.getActive());
        dao.setUsername(userDetail.getUsername());
        dao.setCreatedAt(userDetail.getCreatedAt());
        dao.setUpdatedAt(userDetail.getUpdatedAt());
        dao.setDeletedAt(userDetail.getDeletedAt());
        dao.setDescription(userDetail.getDescription());

        List<String> authorities = userDetail.getAuthorities();
        if (authorities == null) {
            throw new BadRequestException("authorities can't be null");
        }
        dao.setAuthorities(authorities.stream().collect(Collectors.joining(", ")));

        return dao;
    }

    private SimpleUserDetail toDto(UserDetailEntity dao) {
        SimpleUserDetail dto = new SimpleUserDetail();
        dto.setId(dao.getId());
        dto.setName(dao.getName());
        dto.setEmail(dao.getEmail());
        dto.setPassword(dao.getPassword());
        dto.setActive(dao.getActive());
        dto.setUsername(dao.getUsername());
        dto.setCreatedAt(dao.getCreatedAt());
        dto.setUpdatedAt(dao.getUpdatedAt());
        dto.setDeletedAt(dao.getDeletedAt());
        dto.setDescription(dao.getDescription());

        String authorities = dao.getAuthorities();
        if (authorities == null) {
            throw new RuntimeException("Unexpected empty authorities");
        }
        List<String> authorityList = Arrays.stream(authorities.split(","))
                .map(String::trim)
                .collect(Collectors.toList());
        dto.setAuthorities(authorityList);

        return dto;
    }
}
