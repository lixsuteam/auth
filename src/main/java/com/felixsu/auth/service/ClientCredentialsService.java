package com.felixsu.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on 12/6/17.
 *
 * @author felixsoewito
 */
@Service
public class ClientCredentialsService {

    private JdbcClientDetailsService clientDetailsService;

    @Autowired
    public ClientCredentialsService(JdbcClientDetailsService clientDetailsService) {
        this.clientDetailsService = clientDetailsService;
    }

    public void save(ClientDetails clientDetails) {
        clientDetailsService.addClientDetails(clientDetails);
    }

    public void update(ClientDetails clientDetails) {
        clientDetailsService.updateClientDetails(clientDetails);
    }

    public void updateClientSecret(String clientId, String clientSecret) {
        clientDetailsService.updateClientSecret(clientId, clientSecret);
    }

    public ClientDetails loadByClientId(String clientId) {
        return clientDetailsService.loadClientByClientId(clientId);
    }

    public List<ClientDetails> loadAll() {
        return clientDetailsService.listClientDetails();
    }
}
