package com.felixsu.auth.persistence.repository;

import com.felixsu.commons.persistence.BaseRepository;
import com.felixsu.auth.persistence.model.UserDetailEntity;

/**
 * Created on 12/13/17.
 *
 * @author felixsoewito
 */
public interface UserDetailRepository extends BaseRepository<UserDetailEntity, Integer> {

    UserDetailEntity findByUsername(String username);
    UserDetailEntity findByEmail(String email);
}
