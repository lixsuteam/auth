INSERT INTO oauth_client_details
	(client_id, client_secret, scope, authorized_grant_types,
	web_server_redirect_uri, authorities, access_token_validity,
	refresh_token_validity, additional_information, autoapprove)
VALUES
	("web_browser_client", "secret", "read,write", "password,refresh_token",
	 null, "ROLE_CLIENT, ROLE_WEB", 3600,
	 36000, null, true);

INSERT INTO oauth_client_details
(client_id, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES
	("android_client", "secret", "read,write", "password,refresh_token",
	 null, "ROLE_CLIENT, ROLE_ANDROID", 3600,
	 36000, null, true);

INSERT INTO oauth_client_details
(client_id, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES
  ("android_read_client", "secret", "read", "client_credentials",
   null, "ROLE_CLIENT, ROLE_ANDROID_READ", 3600,
   36000, null, true);

INSERT INTO oauth_client_details
(client_id, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES
	("trusted_client", "secret", "read,write", "client_credentials",
	 null, "ROLE_CLIENT, ROLE_TRUSTED", 3600,
	 36000, null, true);

INSERT INTO user_details
(id, username, password, name,
 email, authorities, is_active,
 description, created_at, updated_at, deleted_at)
VALUES
	(1, "admin", "$2a$11$qIaep8xyktGkHi1Syh23M.HIUdkvXo2MfoB9UoxYBp2MM.c7K4vdO", "admin name",
	"admin@localhost", "ROLE_ALL,ROLE_ADMIN", 1,
	"Admin Automatically created", "2017-12-13 20:20:21", NULL, NULL);
